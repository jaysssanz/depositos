﻿namespace notificaciones.DTOs
{
    public class NotificacionRequest
    {
        public int IdR { get; set; }
        public int IdCuentaR { get; set; }
        public string TipoR { get; set; }
        public decimal ValorR { get; set; }
        public string ClienteR { get; set; }
    }
}
