﻿using depositos.DTOs;
using depositos.Models;

namespace depositos.Services
{
    public interface IServiceAccount
    {
        Task<bool> DepositAccount(AccountRequest request);
        bool Execute(Transaction request);
    }
}
