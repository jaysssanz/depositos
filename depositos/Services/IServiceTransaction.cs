﻿using depositos.Models;

namespace depositos.Service
{
    public interface IServiceTransaction
    {
        Task<Transaction> Deposit(Transaction transaction);
    }
}

