﻿    using depositos.DTOs;
    using depositos.Models;
    using depositos.Service;

    namespace depositos.Services
    {
        public class ServiceAccount : IServiceAccount
        {
            //Inyeccion de dependencia

            private readonly IConfiguration _configuration;
            private readonly IServiceTransaction _transactionService;
            private readonly IHttpClient _httpClient;

            //--

            public ServiceAccount(IConfiguration configuration, IServiceTransaction transactionService, IHttpClient httpClient)
            {
                _configuration = configuration;
                _transactionService = transactionService;
                _httpClient = httpClient;
            }

            public async Task<bool> DepositAccount(AccountRequest request)
            {
                string uri = _configuration["proxy:urlAccountDeposit"];
                var response = await _httpClient.PostAsync(uri, request);
                response.EnsureSuccessStatusCode();
                return true;

            }

            public bool Execute(Transaction request)
            {
                bool response = false;
                AccountRequest account = new AccountRequest()
                {
                    Amount = request.Amount,
                    IdAccount = request.AccountId
                };
                response = DepositAccount(account).Result;
                return response;
            }
        }
    }
